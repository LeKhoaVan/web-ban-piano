package iuh.dhktpm15b.productservice.controller;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import iuh.dhktpm15b.productservice.model.Product;
import iuh.dhktpm15b.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/findAll")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/findById")
    @Retry(name = "productService")
    @CircuitBreaker(name = "productServiceCircuitb")
    @Cacheable(value = "product", key = "#id")
    public Product findById(@RequestParam("id") String id){

        return productService.findById(id);
    }


    @PostMapping("/create")
    public ResponseEntity<?> createProduct(@RequestBody Product product){
        return new ResponseEntity<>(productService.save(product), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete")
    @CacheEvict(value = "product", key = "#id")
    //xóa toàn bộ trong redis thay key = "#id" thành allEntries = true
    public void deleteById(@RequestParam("id") String id){
        productService.deleteById(id);
    }


}
