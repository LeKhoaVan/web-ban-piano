package iuh.dhktpm15b.productservice.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.*;
import java.io.Serializable;


@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = ProductEntity.Product.TABLE_NAME)
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = ProductEntity.Product.ID_PRODUCT)
    private String id;

    @Column(name = ProductEntity.Product.NAME_PRODUCT)
    private String name;

    @Column(name = ProductEntity.Product.CATEGORY)
    private String category;

    @Column(name = ProductEntity.Product.PRICE_PRODUCT)
    private float price;

    @Column(name = ProductEntity.Product.QUANTITY_PRODUCT)
    private int quantity;

    @Column(name = ProductEntity.Product.IMG_PRODUCT)
    private String img;
}
