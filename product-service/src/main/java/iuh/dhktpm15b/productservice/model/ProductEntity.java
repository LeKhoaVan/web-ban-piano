package iuh.dhktpm15b.productservice.model;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ProductEntity {
    public static class Product{
        public static final String TABLE_NAME = "product";
        public static final String ID_PRODUCT = "p_id";
        public static final String NAME_PRODUCT = "p_name";
        public static final String CATEGORY = "p_category";
        public static final String PRICE_PRODUCT = "p_cost";
        public static final String QUANTITY_PRODUCT = "p_quantity";
        public static final String IMG_PRODUCT = "p_img";

    }
}
