package iuh.dhktpm15b.productservice.service;

import iuh.dhktpm15b.productservice.model.Product;
import iuh.dhktpm15b.productservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;


    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product findById(String id) {
        return productRepository.findById(id).orElseThrow(() -> new RuntimeException("error findById of product"));
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public boolean deleteById(String id) {
        Product pr = productRepository.findById(id).orElse(null);
        if(pr != null) {
            productRepository.deleteById(id);
            return true;
        }

        return false;

    }
}
