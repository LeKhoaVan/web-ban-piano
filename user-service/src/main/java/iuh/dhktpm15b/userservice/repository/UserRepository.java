package iuh.dhktpm15b.userservice.repository;

import iuh.dhktpm15b.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    @Query(nativeQuery = true, value = "select u_username, u_fullname, u_password, u_address from user where u_username = :username")
    User findByName(@Param("username") String username);

    @Query(nativeQuery = true, value = "select * from user where u_username = :username")
    User findByUserName(@Param("username") String userName);
}
