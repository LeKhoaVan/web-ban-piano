package iuh.dhktpm15b.userservice.repository;

import iuh.dhktpm15b.userservice.model.UserGroupRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGroupRoleRepository extends JpaRepository<UserGroupRole, Integer> {

    @Query(nativeQuery = true, value = "insert into user_role(user_id,role_id) values(:userName, :code)")
    public UserGroupRole saveOne(@Param("userName") String userName, @Param("code") String roleId);

    @Query(nativeQuery = true, value = "select * from user_role where u_username = :username")
    public List<UserGroupRole> findRoleByUsername(@Param("username") String username);

}
