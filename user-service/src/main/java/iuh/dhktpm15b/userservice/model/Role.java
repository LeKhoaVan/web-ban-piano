package iuh.dhktpm15b.userservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserEntity.RoleEntity.TABLE_NAME)
public class Role  implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = UserEntity.RoleEntity.ID)
    private String code;

    @Column(name = UserEntity.RoleEntity.NAME, nullable = false, unique = true)
    private String name;

    @Column(name = UserEntity.RoleEntity.DESCRIPTION, nullable = false)
    private String description;

    @OneToMany(mappedBy = UserEntity.RoleEntity.TABLE_NAME, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<UserGroupRole> userGroupRoles;
}
