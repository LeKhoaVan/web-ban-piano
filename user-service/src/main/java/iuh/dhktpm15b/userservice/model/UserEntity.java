package iuh.dhktpm15b.userservice.model;

import lombok.experimental.UtilityClass;


@UtilityClass
public class UserEntity {
    public static final String TABLE_NAME = "user";
    public static final String ID = "u_username";
    public static final String NAME = "u_fullname";
    public static final String PASSWORD = "u_password";
    public static final String ADDRESS = "u_address";

    public static class RoleEntity {
        public static final String TABLE_NAME = "role";
        public static final String ID = "r_code";
        public static final String NAME = "r_name";
        public static final String DESCRIPTION = "r_description";
    }

    public static class UserRole{
        public static final String USER_ROLE = "user_role";
        public static final String USER_ID = "u_username";
        public static final String ROLE_ID = "r_code";
        public static final String ID = "user_role_id";
    }



}
