package iuh.dhktpm15b.userservice.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserEntity.TABLE_NAME)
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = UserEntity.ID)
    private String userName;

    @Column(name = UserEntity.NAME, nullable = false)
    private String fullName;

    @Column(name = UserEntity.PASSWORD, nullable = false)
    private String password;

    @Column(name = UserEntity.ADDRESS)
    private String address;

    @OneToMany(mappedBy = UserEntity.TABLE_NAME, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<UserGroupRole> userGroupRoles;
}
