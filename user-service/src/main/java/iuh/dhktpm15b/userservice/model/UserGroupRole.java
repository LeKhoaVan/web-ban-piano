package iuh.dhktpm15b.userservice.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserEntity.UserRole.USER_ROLE)
public class UserGroupRole  implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = UserEntity.UserRole.ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = UserEntity.UserRole.USER_ID)
    private User user;

    @ManyToOne
    @JoinColumn(name = UserEntity.UserRole.ROLE_ID)
    private Role role;

    public void addAuthority(User user, Role role){

    }


}
