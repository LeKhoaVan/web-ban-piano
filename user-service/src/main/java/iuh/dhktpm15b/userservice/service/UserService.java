package iuh.dhktpm15b.userservice.service;

import iuh.dhktpm15b.userservice.model.User;
import iuh.dhktpm15b.userservice.model.UserGroupRole;
import iuh.dhktpm15b.userservice.repository.UserGroupRoleRepository;
import iuh.dhktpm15b.userservice.repository.UserRepository;
import iuh.dhktpm15b.userservice.security.dto.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserGroupRoleRepository userGroupRoleRepository;

    ModelMapper modelMapper = new ModelMapper();

    public UserDTO createUser(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserDTO userDTO = modelMapper.map(userRepository.save(user), UserDTO.class) ;
        return userDTO;
    }

    public List<UserDTO> findAll(){
//       List<UserDTO> userDTO = userRepository.findAll().stream()
//                .map(model -> modelMapper.map(model, UserDTO.class))
//                .collect(Collectors.toList());

        List<UserDTO> userDTOList = new ArrayList<>();
        List<User> users = userRepository.findAll();
        for(User user : users){
            List<String> roles = userGroupRoleRepository.findRoleByUsername(user.getUserName()).stream()
                    .map(model -> model.getRole().getName())
                    .collect(Collectors.toList());

            UserDTO userDTO = UserDTO.builder()
                    .userName(user.getUserName())
                    .fullName(user.getFullName())
                    .address(user.getAddress())
                    .groupRole(roles)
                    .build();
            userDTOList.add(userDTO);

        }
        return userDTOList;
    }

    public UserDTO findRoleByUsername(String userName){
        List<UserGroupRole> groupRoles= userGroupRoleRepository.findRoleByUsername(userName);
        List<String> roleNames = groupRoles.stream()
                .map(m -> m.getRole().getName()).toList();
        
        User user = userRepository.findByUserName(userName);
        UserDTO userDTO = UserDTO.builder()
                .userName(user.getUserName())
                .fullName(user.getFullName())
                .address(user.getAddress())
                .groupRole(roleNames)
                .build();

        return userDTO;
    }



}
