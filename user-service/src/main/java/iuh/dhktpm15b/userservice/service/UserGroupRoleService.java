package iuh.dhktpm15b.userservice.service;

import iuh.dhktpm15b.userservice.model.Role;
import iuh.dhktpm15b.userservice.model.User;
import iuh.dhktpm15b.userservice.model.UserGroupRole;
import iuh.dhktpm15b.userservice.repository.UserGroupRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserGroupRoleService {
    @Autowired
    UserGroupRoleRepository userGroupRoleRepository;
    public UserGroupRole create(String userName, String code) {
        User user = User.builder()
                .userName(userName)
                .build();

        Role role = Role.builder()
                .code(code)
                .build();

        UserGroupRole userGroupRole = UserGroupRole.builder()
                .user(user)
                .role(role)
                .build();

        return userGroupRoleRepository.save(userGroupRole);
    }
}
