package iuh.dhktpm15b.userservice.security.auth;


import iuh.dhktpm15b.userservice.model.User;
import iuh.dhktpm15b.userservice.model.UserGroupRole;
import iuh.dhktpm15b.userservice.repository.UserGroupRoleRepository;
import iuh.dhktpm15b.userservice.repository.UserRepository;
import iuh.dhktpm15b.userservice.security.dto.UserDTO;
import iuh.dhktpm15b.userservice.security.jwt.JwtUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtils jwtUtils;

    @Autowired
    private UserGroupRoleRepository userGroupRoleRepository;

    AuthService(UserRepository userRepository,  PasswordEncoder passwordEncoder, JwtUtils jwtUtils) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtils = jwtUtils;
    }


    public String login(User user) {
        User user2 = userRepository.findById(user.getUserName())
                .orElse(null);

        if(passwordEncoder.matches(user.getPassword(), user2.getPassword())){
            // return jwt
            return jwtUtils.generateJwt(user.getUserName());
        }

        return "error login";
    }

    public UserDTO getUserByToken(String token){
        ModelMapper modelMapper = new ModelMapper();

        if (jwtUtils.validateJwt(token)) {
            String username = jwtUtils.getUsername(token);
            List<UserGroupRole> groupRoleList = userGroupRoleRepository.findRoleByUsername(username);
            List<String> roleNames = groupRoleList.stream()
                    .map(u -> u.getRole().getName()).toList();

            User user = userRepository.findByName(username);

            UserDTO userDTO = UserDTO.builder()
                    .userName(user.getUserName())
                    .fullName(user.getFullName())
                    .address(user.getAddress())
                    .groupRole(roleNames)
                    .build();

            return userDTO;
        }
        return null;
    }
}

