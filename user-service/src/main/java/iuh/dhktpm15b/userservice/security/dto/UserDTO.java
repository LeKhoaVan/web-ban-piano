package iuh.dhktpm15b.userservice.security.dto;

import iuh.dhktpm15b.userservice.model.UserGroupRole;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO  implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userName;
    private String fullName;
    private String address;
    private List<String> groupRole;
}
