package iuh.dhktpm15b.userservice.security.auth;

import iuh.dhktpm15b.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/auth")
public class AuthRestController {
    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?>login(@RequestBody User user){
        return new ResponseEntity<>(authService.login(user), HttpStatus.OK);
    }

    @GetMapping("/getUser")
    public ResponseEntity<?> getUser(@RequestParam("token") String token){
        return new ResponseEntity<>(authService.getUserByToken(token), HttpStatus.OK);
    }

}
