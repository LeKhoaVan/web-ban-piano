package iuh.dhktpm15b.userservice.Controller;

import iuh.dhktpm15b.userservice.model.UserGroupRole;
import iuh.dhktpm15b.userservice.service.UserGroupRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/authority")
public class AuthorityController {
    @Autowired
    UserGroupRoleService userGroupRoleService;

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestParam("username") String userName, @RequestParam("code") String code){
        return new ResponseEntity<>(userGroupRoleService.create(userName, code), HttpStatus.CREATED);
    }
}
