package iuh.dhktpm15b.userservice.Controller;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import iuh.dhktpm15b.userservice.model.User;
import iuh.dhktpm15b.userservice.security.dto.UserDTO;
import iuh.dhktpm15b.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody User user){
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED);
    }

    @GetMapping("/findAll")
    @Retry(name = "userService")

    @Cacheable(value = "users", key = "#id")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/findRoleByUsername")
    @Retry(name = "userService")
    @CircuitBreaker(name = "userServiceCircuitb")
    @Cacheable(value = "user", key = "#username")
    public UserDTO findRoleByUsername(@RequestParam("username") String username){
        return userService.findRoleByUsername(username);
    }
}
